import com.squareup.moshi.*
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

inline class UserId(val value: String):Comparable<UserId> {
    override fun compareTo(other: UserId): Int {
        return value.compareTo(other.value)
    }
}

data class UserProfile(val first: String, val last: String)

@ExperimentalStdlibApi
fun main(args: Array<String>) {
    val userMap = hashMapOf<UserId, UserProfile>(
        UserId("owen") to UserProfile("歐", "文"),
        UserId("shyhjie") to UserProfile("士", "捷")
    )
    val adapter = Moshi.Builder()
        .addLast(KotlinJsonAdapterFactory())
        .addAdapter(object:JsonAdapter<UserId>(){
            override fun fromJson(reader: JsonReader): UserId? {
                return UserId(reader.nextString())
            }

            override fun toJson(writer: JsonWriter, value: UserId?) {
                writer.value(value?.value)
            }

        })
        .build().adapter<Map<UserId, UserProfile>>(
        Types.newParameterizedType(Map::class.java, UserId::class.java, UserProfile::class.java)
    )

    val json = adapter.toJson(userMap)
    println(json)
    val hashMap = adapter.fromJson(json)
    println(hashMap)
}